// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app=angular.module('app', ['ionic','ngCordova']);

var db = null;

app.config(function($stateProvider,$urlRouterProvider){

  $stateProvider
  .state('menu',{
    url: '/menu',
    abstract: true,
    templateUrl: 'menu.html',
    controller: 'menuCtrl'
  })
  .state('menu.cadastrar',{
      url: '/cadastro',
      views: {'menuContent': { 
        templateUrl: 'cadastro.html',
        controller: 'appCtrl'
      }}    
  })
  .state('menu.pesquisar',{
      url: '/pesquisar',
      cache: false,
      views: {'menuContent': {
        templateUrl: 'pesquisa.html',
        controller: 'appCtrl'
      }}
  })
  .state('menu.detalhar',{
      url: '/detalhar/:codigo',
      views: {'menuContent': {
        templateUrl: 'detalhes.html',
        controller: 'dtlCtrl'
      }}  
  });
  $urlRouterProvider.otherwise('/menu/cadastro'); 
});

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }

    //must be this style (no sqlitePlugin)
    db = window.openDatabase('clientes.db','1.0','clientes.db',200000);

    db.transaction(function(tx){
      tx.executeSql('CREATE TABLE IF NOT EXISTS clientes(codigo integer primary key autoincrement,nome text,sobrenome text)');
    },function(error){
      window.alert('Erro ao criar tabela!!');
    });  

  });
});

//Controller of app menu...
app.controller('menuCtrl',function($scope,$ionicSideMenuDelegate){
  
  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };  

});

app.controller('dtlCtrl',function($scope,$stateParams,$cordovaToast){
 
   $scope.pessoa = {};

   $scope.excluir = function(){
    db = window.openDatabase('clientes.db','1.0','clientes.db',200000);
    db.transaction(function(tx){
      tx.executeSql("delete from clientes where codigo=?",[$stateParams.codigo],function(tx,res){
        $cordovaToast.show("Excluído com sucesso",'short','center');
      },function(error){
        $cordovaToast.show("Erro na exclusão",'long','center');
      });
    });      
   }

   $scope.atualizar = function(){
    db = window.openDatabase('clientes.db','1.0','clientes.db',200000);
    db.transaction(function(tx){
      tx.executeSql("update clientes set nome=?, sobrenome=? where codigo=?",
        [$scope.pessoa.nome,$scope.pessoa.sobrenome,$stateParams.codigo],function(tx,res){
          $cordovaToast.show("Atualizado com sucesso",'short','center');
        },function(error){
          $cordovaToast.show("Erro na atualização",'long','center');
      });
    });      
   }

   $scope.detalhar = function(){
    db = window.openDatabase('clientes.db','1.0','clientes.db',200000);
    db.transaction(function(tx){
      tx.executeSql("Select * from clientes where codigo=?",[$stateParams.codigo],function(tx,res){
        $scope.pessoa = res.rows.item(0);
      },function(error){
      });
    });        
   }

   $scope.detalhar();

});

app.controller('appCtrl',function($scope,$cordovaToast,$state){

  $scope.data={};
  $scope.pessoas=[];
  $scope.perTab=10;
  $scope.total = 0;

  $scope.listar = function(){
    $scope.pessoas=[];
    db = window.openDatabase('clientes.db','1.0','clientes.db',200000);
    db.transaction(function(tx){
      tx.executeSql("Select * from clientes order by nome",[],function(tx,res){
        if (res.rows.length != 0){
          for (var i=0; i < res.rows.length; i++){
            $scope.pessoas.push(res.rows.item(i));
          }  
        }      
        $scope.total = $scope.pessoas.length;
      },function(error){
        $cordovaToast.show("Erro de pesquisa",'long','center');
      });
    });   
  };

  $scope.inserir = function(){
    db = window.openDatabase('clientes.db','1.0','clientes.db',200000);
    if (angular.isDefined($scope.data.nome) && angular.isDefined($scope.data.sobrenome)){
      var nome = $scope.data.nome;
      var sobrenome = $scope.data.sobrenome;  
      db.transaction(function(tx){
        tx.executeSql("Insert into clientes(nome,sobrenome) values(?,?)",[nome,sobrenome],function(tx,res){
          $cordovaToast.show("Inserido com sucesso",'short','center');
        },function(error){
          $cordovaToast.show("Erro ao inserir os dados",'short','center');
        });
      });
    }  
    $scope.data.nome="";
    $scope.data.sobrenome="";
  };

  $scope.atualizar = function(){ 
    if ($scope.perTab <= $scope.total){
      $scope.perTab+=10;
    }  
    $scope.$broadcast('scroll.infiniteScrollComplete'); 
  }
  
  $scope.listar();

});